<?php

error_reporting(E_ALL);

$app = require_once 'lib/App.php';


// список
$app->route()->name('index')->to(function($app) {

	$app->stash->items = $app->items();

	$app->render(array(
		'html' => function($app) {
			include 'templates/items.php';
		},
		'json' => $app->stash->items(),
	));
});

// добавление нового элемента - форма
$app->route_get('new')->name('new')->to(function($app) {
	$app->render(array(
		'html' => function($app) {
			include 'templates/form.php';
		},
	));
});
// добавление нового элемента
$app->route_post('new')->to(function($app) {

	$error = $app->item_save(array(
		'name' => $app->args->name,
		'dump' => $app->args->dump,
	));

	if   ($error) {

		$app->stash->error = $error;

		$app->render(array(
			'html' => function($app) {
				include 'templates/form.php';
			},
			'json' => $app->stash->error(),
		));
	}
	else	$app->redirect('index');
});

// редактирование элемента - форма
$app->route_get('#id')->name('item')->to(function($app) {

	$item = $app->item_get($app->args->id);
	if (! $item) return FALSE;

	$app->stash->item = $item;

	$app->render(array(
		'html' => function($app) {
			include 'templates/form.php';
		},
		'json' => function($app) use ($item) {
			$item['dump'] = json_decode($item['dump']);
			return $item;
		},
	));
});
// редактирование элемента
$app->route_post('#id')->to(function($app) {

	$id    = $app->args->id;

	if   (! $app->item_get($id)) return FALSE;

	$item  = array(
		'name' => $app->args->name,
		'dump' => $app->args->dump,
	);
	$error = $app->item_save($item, $id);

	if   (  $error) {

		$app->stash->item  = $item;
		$app->stash->error = $error;

		$app->render(array(
			'html' => function($app) {
				include 'templates/form.php';
			},
		));
	}
	else	$app->redirect('index');
});

// удаление элемента
$app->route('#id/del')->name('item_del')->to(function($app) {
	if   ($app->item_del($app->args->id))
		$app->redirect('index');
	else
		return FALSE;
});


// 404
if (! $app->route_match())
	$app->redirect('index');

?>
