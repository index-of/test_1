<?php

require_once 'Lava.php';

// --- app ------------------------------------------------------------------ //

class App extends Lava\App {

	// список
	public function items () {

		$path  = $this->item_path();

		$dh    = @opendir($path);
		if (! $dh) return;

		$items = array();

		while (($file = readdir($dh)) !== FALSE)
			if (preg_match('/^(\w+)\.php$/', $file, $match)) {
				$data = require "${path}/${file}";
				$items[$match[1]] = $data['name'];
			}

		closedir($dh);

		return $items;
	}

	// получение
	public function item_get ($id) {

		$path = $this->item_path($id);

		if (is_readable($path))
			return require $path;
	}
	// удаление
	public function item_del ($id) {

		$path = $this->item_path($id);

		if (is_readable($path))
			return unlink ($path);
	}

	// путь
	public function item_path ($id = NULL) {

		$path = array($this->conf->dir_data);
		if ($id) $path[] = "${id}.php";

		return $this->home($path);
	}

	// сохранение
	public function item_save ($data, $id = NULL) {

		$path  = $this->item_path();

		if     (! is_writable($path))
			die("Directory is not writable: ${path}");

		// присваиваем айдишник
		if     (! $id) $id = $this->safe->uuid();

		$error = NULL;

		// проверяем на пустоту
		if     (! isset($data['name']))
			$error['name'] = 'null';
		if     (! isset($data['dump']))
			$error['dump'] = 'null';
		// просто на валидность JSON
		// структуру не смотрю
		elseif (! json_decode($data['dump']))
			$error['dump'] = 'invalid';

		if     (  $error)
			return  $error;
		else   {
			$path = $this->item_path($id);
			$ok   = @file_put_contents($path, sprintf(
				'<?php return %s ?>',
				var_export($data, TRUE)
			));
			if (! $ok) die("I can not write a file: ${path}");
		}
	}
}

// --- создаем приложение --------------------------------------------------- //

return new App (array(
	'type'     => 'html',
	'charset'  => 'utf-8',
	'dir_data' => 'data',
));

?>
