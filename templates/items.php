<?php
	if (! isset($app)) return;

	include 'templates/_header.php';
?>
<a href="<?php echo $app->uri('new') ?>"><strong>Добавить</strong></a>
<ul>
	<?php foreach ($app->stash->items() as $id => $name): ?>
		<li>
			<a href="<?php echo $app->uri('item',     array('id' => $id)) ?>"><?php echo htmlspecialchars($name) ?></a>
			|
			<a href="<?php echo $app->uri('item',     array('id' => $id)) ?>.json" target="_blank"><small>JSON</small></a>
			|
			<a href="<?php echo $app->uri('item_del', array('id' => $id)) ?>"><small>удалить</small></a>
		</li>
	<?php endforeach; ?>
</ul>
<?php
	include 'templates/_footer.php';
?>
