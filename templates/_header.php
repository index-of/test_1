<?php
	if (! isset($app)) return;
?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="<?php echo $app->conf->charset ?>">
		<title>Composite</title>
		<link href="<?php echo $app->pub('css/main.css') ?>" rel="stylesheet">
	</head>
	<body>
