<?php
	if (! isset($app)) return;

	$item    = $app->stash->item();

	$args    = $app->args;
	$is_post = $app->env->is_post;

	$error   = $app->stash->error();

	include 'templates/_header.php';
?>
<h3><?php echo $item ? 'Редактирование' : 'Новый' ?></h3>
<form method="POST">
	<div class="form-control">
		<label for="name">Название</label>
		<input id="name" type="text" name="name" value="<?php echo htmlspecialchars($is_post || ! $item ? $args->name : $item['name']) ?>">
		<?php if (isset($error['name'])): ?>
			<div class="error"><?php echo $error['name'] ?></div>
		<?php endif; ?>
	</div>
	<div class="form-control">
		<label for="dump">Компоновщик</label>
		<input id="dump" type="text" name="dump" value="<?php echo htmlspecialchars($is_post || ! $item ? $args->dump : $item['dump']) ?>">
		<?php if (isset($error['dump'])): ?>
			<div class="error"><?php echo $error['dump'] ?></div>
		<?php endif; ?>
	</div>
	<button type="submit"><?php echo $item ? 'Сохранить' : 'Добавить' ?></button>
	<a href="<?php echo $app->uri('index') ?>">Отмена</a>
</form>
<?php
	include 'templates/_footer.php';
?>
