Папка data должна быть доступна для записи

Если запускается под apache+mod_rewrite и скрипт не в корневой папке, то проставить путь в .htaccess для RewriteBase

Если запускается под nginx+php_fpm то конфиг в виде


```
#!nginx

server {
	...
	root ...;

	location ~ \.php$ {
		fastcgi_pass	127.0.0.1:9000;
		fastcgi_param	SCRIPT_FILENAME $document_root/index.php;
		include		fastcgi_params;
	}
	location / {
		try_files $uri /index.php?$args;
	}
}
```